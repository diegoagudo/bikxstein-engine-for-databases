<?php

define('PROGRAM_EXECUTION_SUCCESSFUL', true);
require_once __DIR__ . '/../vendor/autoload.php';

$app = new APIDB\App();
$app->run();

