<?php

/**
 * 
 * Recebe as rotas e executa.
 * 
 * @author Bruno Akio Kawakami
 */ 

namespace APIDB\Controller;


class Content extends AbstractController {

    protected $_alias = '/content';

    public function __construct(\Phlyty\App $phlyty, $connArray = null, $secretArray = array(), $controller = null) {
        
        parent::__construct($phlyty, $connArray, $secretArray, $controller);
        
    }

}