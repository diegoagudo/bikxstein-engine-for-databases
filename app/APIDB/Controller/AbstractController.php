<?php


namespace APIDB\Controller;
use APIDB\Response,
    APIDB\Helper\ErrorHandling,
    APIDB\Receive;
/**
 * Description of AbstractController
 *
 * @author Bruno Akio Kawakami
 */
class AbstractController {
    
    protected $_phlyty = null;
    protected $_table = null;
    protected $_response = null;
    protected $_receive = null;
    protected $_errorHandling = null;
    protected $_connArray = null;
    protected $_controller = null;
    protected $_config = null;
    protected $_model;


    public function __construct(\Phlyty\App $phlyty, $connArray = null, $secretArray = array(), $controller = null, $model = null) {


        $this->_config = $this->getConfig();


        $this->_phlyty = $phlyty;
        $this->_model = $model;
        $this->_controller = $controller;
        
        $this->_response = new Response($this->_phlyty, $secretArray);
        $this->_response->encrypt = $this->_config['encrypt']['response'];
        
        
        $this->_receive = new Receive($secretArray);
        $this->_receive->decrypt = $this->_config['decrypt']['receive'];
        
        $this->_errorHandling = new ErrorHandling($this->_config['error_handling']['emailTo'],$this->_config['error_handling']['emailFrom']);
        $this->_errorHandling->_typeHandling = $this->_config['error_handling']['type'];
        $this->_connArray = $connArray;
        
        $modelClass = "\\APIDB\\Model\\".$controller."Table";
        
        $this->_model = new $modelClass($this->_connArray);
        
        
    }

    
    public function run() {

        $this->_phlyty->get($this->_alias, function() {

                    try {
                        
                        $dataGet = $_GET;

                        $page = empty($dataGet['page']) || !is_numeric($dataGet['page']) ? 1 : (real) $dataGet['page'];
                        $perPage = empty($dataGet['perPage']) || !is_numeric($dataGet['perPage']) ? 15 : (real) $dataGet['perPage'];
                        
                        

                        $dataRetriver = $this->_model->getAll($page, $perPage);

                       
                        
                        if (empty($dataRetriver)) {

                            $statusNumber = 206;
                            $this->_response->responsePrependArray['results'] = 'No data content';
                        } else {

                            $statusNumber = 200;
                        }
                        
                        $this->_response->set('statusNumber', $statusNumber);
                        $this->_response->processDataReturn($dataRetriver);
                                
                        
                    } catch (\Exception $e) {

                        $this->_errorHandling->handling($e);

                        $dataRetriver = array(
                            'error' => 'unknow exception'
                        );

                        $this->_response->set('statusNumber', 400)->processDataReturn($dataRetriver);
                    }
                });

        $this->_phlyty->get($this->_alias.'/id/:id', function() {

                    try {

                        $id = $this->_phlyty->params()->getParam('id');

                        $dataRetriver = $this->_model->getByKey($id);

                        if (empty($dataRetriver)) {

                            $statusNumber = 206;
                            $this->_response->responsePrependArray['results'] = 'No data content';
                        } else {

                            $statusNumber = 200;
                        }

                        $this->_response->set('statusNumber', $statusNumber);
                        $this->_response->processDataReturn($dataRetriver);
                    } catch (\Exception $e) {

                        $this->_errorHandling->handling($e);

                        $dataRetriver = array(
                            'error' => 'unknow exception'
                        );

                        $this->_response->set('statusNumber', 400)->processDataReturn($dataRetriver);
                    }
                });

        $this->_phlyty->get($this->_alias.'/type/:type/latency/:latency/page/:page/perpage/:perpage/columns/:columns', function() {

                    try {

                        $columnsArray = array();

                        $columns = $this->_phlyty->params()->getParam('columns');
                        $type = $this->_phlyty->params()->getParam('type');
                        $latency = $this->_phlyty->params()->getParam('latency');
                        $perPage = $this->_phlyty->params()->getParam('perpage');
                        $page = $this->_phlyty->params()->getParam('page');

                        $type = strtoupper($type);
                        $latency = strtoupper($latency);
                        
                        parse_str($columns, $columnsArray);
                        
                        $perPage = !is_numeric($perPage) ? 15 : $perPage; 
                        
                        $page = !is_numeric($page) ? 1 : $page;
                        

                        $dataRetriver = $this->_model->getByColumns($columnsArray, $type, $latency, $page, $perPage);

                        if (empty($dataRetriver) || empty($columnsArray)) {

                            $statusNumber = 206;
                            $this->_response->responsePrependArray['results'] = 'No data content';
                        } else {

                            $statusNumber = 200;
                        }

                        $this->_response->set('statusNumber', $statusNumber);
                        $this->_response->processDataReturn($dataRetriver);
                    } catch (\Exception $e) {

                        $this->_errorHandling->handling($e);

                        $dataRetriver = array(
                            'error' => 'unknow exception'
                        );

                        $this->_response->set('statusNumber', 400)->processDataReturn($dataRetriver);
                    }
                });

        $this->_phlyty->post($this->_alias, function() {

                    try {

                        $dataRaw = $this->_receive->getPostMem();

                        $dataArrayContent = json_decode($dataRaw, true);

                        
                        $classRules = "APIDB\\Rules\\" . $this->_controller."Rules";

                        $objRules = new $classRules($dataArrayContent);

                        $dataVerification = $objRules->insertRules();

                        if ($dataVerification['error'] == false) {

                            $idReturn = $this->_model->save($dataVerification['dataReturn']);

                            $this->_response->set('statusNumber', 200);
                            $this->_response->processDataReturn($idReturn);
                            
                        } else {

                            $this->_response->set('statusNumber', 400);
                            $this->_response->processDataReturn($dataVerification['dataReturn']);
                            
                        }
                    } catch (\Exception $e) {

                        $this->_errorHandling->handling($e);

                        $dataRetriver = array(
                            'error' => 'unknow exception'
                        );

                        $this->_response->set('statusNumber', 400);
                        $this->_response->processDataReturn($dataRetriver);
                    }
                });

        $this->_phlyty->put($this->_alias.'/id/:id', function() {

                    try {
                        
                        $dataRaw = $this->_receive->getPostMem();

                        $id = $this->_phlyty->params()->getParam('id');

                        $dataArrayContent = json_decode($dataRaw, true);

                        $classRules = "APIDB\\Rules\\" . $this->_controller;

                        if (is_array($dataArrayContent)) {

                            $objRules = new $classRules($dataArrayContent);

                            $dataVerification = $objRules->updateRules();
                            
                        } else {
                            $this->_response->set('statusNumber', 400);
                            $this->_response->processDataReturn(null);
                        }



                        if ($dataVerification['error'] == false) {


                            $this->_model->updateByKey($id, $dataVerification['dataReturn']);


                            $this->_response->set('statusNumber', 200);
                            $this->_response->processDataReturn();
                        } else {

                            $this->_response->set('statusNumber', 400);
                            $this->_response->processDataReturn($dataVerification['dataReturn']);
                        }
                    } catch (\Exception $e) {

                        $this->_errorHandling->handling($e);

                        $dataRetriver = array(
                            'error' => 'unknow exception'
                        );

                        $this->_response->set('statusNumber', 400);
                        $this->_response->processDataReturn($dataRetriver);
                    }
                });

        $this->_phlyty->put($this->_alias.'/columns/:columns', function() {

                    try {

                        $dataRaw = $this->_receive->getPostMem();

                        $columnsArray = array();

                        $columns = $this->_phlyty->params()->getParam('columns');

                        parse_str($columns, $columnsArray);

                        $dataArrayContent = json_decode($dataRaw, true);

                        $classRules = "APIDB\\Rules\\" . $this->_controller;

                        if (is_array($dataArrayContent)) {

                            $objRules = new $classRules($dataArrayContent);

                            $dataVerification = $objRules->updateRules();
                        } else {
                            $this->_response->set('statusNumber', 400);
                            $this->_response->processDataReturn(null);
                        }

                        if ($dataVerification['error'] == false) {


                            $this->_model->updateByColumns($columnsArray, $dataVerification['dataReturn']);


                            $this->_response->set('statusNumber', 200);
                            $this->_response->processDataReturn();
                        } else {

                            $this->_response->set('statusNumber', 400);
                            $this->_response->processDataReturn($dataVerification['dataReturn']);
                        }
                    } catch (\Exception $e) {

                        $this->_errorHandling->handling($e);

                        $dataRetriver = array(
                            'error' => 'unknow exception'
                        );

                        $this->_response->set('statusNumber', 400);
                        $this->_response->processDataReturn($dataRetriver);
                    }
                });

        $this->_phlyty->delete($this->_alias.'/id/:id', function() {
                    
                    try {

                        $id = $this->_phlyty->params()->getParam('id');

                        $dataRetriver = $this->_model->deleteByKey($id);


                        $this->_response->set('statusNumber', 200);
                        $this->_response->processDataReturn();
                    } catch (\Exception $e) {

                        $this->_errorHandling->handling($e);

                        $dataRetriver = array(
                            'error' => 'unknow exception'
                        );

                        $this->_response->set('statusNumber', 400)->processDataReturn($dataRetriver);
                    }
                });

        $this->_phlyty->delete($this->_alias.'/columns/:columns', function() {

                    try {

                        $columnsArray = array();

                        $columns = $this->_phlyty->params()->getParam('columns');

                        parse_str($columns, $columnsArray);

                        $dataRetriver = $this->_model->deleteByColumns($columnsArray);

                        $this->_response->set('statusNumber', 200);
                        $this->_response->processDataReturn();
                        
                    } catch (\Exception $e) {

                        $this->_errorHandling->handling($e);

                        $dataRetriver = array(
                            'error' => 'unknow exception'
                        );

                        $this->_response->set('statusNumber', 400)->processDataReturn($dataRetriver);
                    }
                });


        $this->_phlyty->run();
    }



    private function getConfig() {

        return include __DIR__ . '/../Config/config.php';
    }

   
}
