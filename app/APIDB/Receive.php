<?php

namespace APIDB;

/**
 * Description of Receive
 *
 * @author Bruno Akio Kawakami
 */
use Zend\Http\PhpEnvironment\Request;
use APIDB\Helper\SecurityCrypt;

class Receive {

    private $_request;
    private $_secretArray;
    private $_securityCrypt;
    public $decrypt = FALSE;

    public function __construct($secretArray = array()) {

        $this->_request = new Request();
        $this->_secretArray = $secretArray;
        $this->_securityCrypt = new SecurityCrypt;
    }

    private function decryptData($encryptData) {

        if ($this->decrypt == TRUE) {

            $key = $this->_securityCrypt->pbkdf2($this->_secretArray['pass'], $this->_secretArray['salt'], 1000, 32);


            $returnData = $this->_securityCrypt->decrypt($encryptData, $key);
        } else {
            $returnData = $encryptData;
        }

        return $returnData;
    }

    public function getHeader($headerName = null) {

        $headerValue = false;

        if ($this->_request->getHeader($headerName)) {
            $headerValue = $this->_request->getHeader($headerName)->getFieldValue();
        }


        return $headerValue;
    }

    public function getURI() {

        $returnURI = null;

        if ($this->_request->getUri()->getPath()) {
            $returnURI = $this->_request->getUri()->getPath();
        }

        return $returnURI;
    }

    public function getPost() {
        $returnArray = array();
        if ($this->_request->getPost()->toArray()) {
            $returnArray = $this->decryptData($this->_request->getPost()->toArray());
        }

        return $returnArray;
    }

    public function getPostMem() {


        $returnArray = $this->decryptData(file_get_contents('php://input'));

        if(empty($returnArray)){
            $returnArray = '{}';
        }
        
        return $returnArray;
    }

}
