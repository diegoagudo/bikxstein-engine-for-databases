<?php

/**
 * Routes mapeia as rotas do projeto
 *
 * @author Bruno Akio Kawakami
 */

namespace APIDB;

class Routes {

    public static function getRoutes() {
        return array(
            '/content' => 'Content',
        );
    }

}
