<?php


namespace APIDB\Helper;

/**
 *
 * @author Bruno Akio Kawakami
 */
class InsertQueryRendering {

    private $_insert;
    private $_values;
    private $_sql;
    private $_prepend = null;
    private $_database;

    public function __construct($database) {
        $this->_database = $database;
    }

    public function insert($table) {
        $this->_insert = "INSERT INTO " . $table . " ";

        return $this;
    }

    public function values($arrayValues) {

        $valuesQuery = array();
        $columns = array_keys($arrayValues);
        $values = array_values($arrayValues);

        foreach ($values as $valuesData) {

            if ($this->_database == "oracle") {
                
                if (eregi("^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$", $valuesData)) {

                    $valuesQuery[] = "to_date('" . $valuesData . "', 'yyyy-mm-dd hh24:mi:ss')";
                } else {

                    $valuesQuery[] = "'" . $valuesData . "'";
                }
                
            }else{
                
                $valuesQuery[] = "'" . $valuesData . "'";
                
            }

        }

        $this->_values = "(" . implode(',', $columns) . ") VALUES (" . implode(',', $valuesQuery) . ") ";
        return $this;
    }

    public function prepend($query) {

        $this->_prepend = $query;

        return $this;
    }

    public function getSQL() {

        $this->_sql = $this->_insert . $this->_values . $this->_prepend;

        return $this->_sql;
    }

}

?>
