<?php


namespace APIDB\Helper;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;

/**
 * Description of CrashEmail
 *
 * @author Bruno Akio Kawakami
 */
class ErrorHandling
{

    public $_typeHandling = 'dump';
    private $_emailTo = null;
    private $_emailFrom = null;

    public function __construct($emailTo, $emailFrom )
    {

        $this->_emailTo = $emailTo;
        $this->_emailFrom = $emailFrom;

    }

    public function handling($exceptionText)
    {
        switch ($this->_typeHandling) {
            case 'dump':

                die($exceptionText);

                break;

            case 'email':

                $this->sendEmail($exceptionText);

                break;

            default:
                break;
        }
    }

    private function sendEmail($exceptionText = null)
    {

        $message = new Message();
        $message->addTo($this->_emailTo)
            ->addFrom($this->_emailFrom)
            ->setSubject('API DATABASE CRASH!')
            ->setBody($exceptionText);

        $transport = new SmtpTransport();

        $transport->send($message);

        return TRUE;
    }

}

?>
