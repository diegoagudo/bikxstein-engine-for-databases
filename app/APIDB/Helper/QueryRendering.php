<?php

namespace APIDB\Helper;
/**
 *
 * @author Bruno Akio Kawakami
 */

use \Doctrine\DBAL\DriverManager;
use APIDB\Helper\InsertQueryRendering;

class QueryRendering {
    
    
    protected $_conn; 
    protected $_database;
    
    public function __construct($connArray, $database = null) {
        
        $conn = DriverManager::getConnection($connArray);
        
        $this->_conn = $conn;
        
        $this->_database = $database;
        
        return $this->_conn;
        
    }
    
    public function selectInit(){
        
        $qb = $this->_conn->createQueryBuilder();
        
        return $qb;
        
    }
    
    public function updateInit(){
        
        $qb = $this->_conn->createQueryBuilder();
        
        return $qb;
        
    }
    
    public function deleteInit(){
        
        $qb = $this->_conn->createQueryBuilder();
        
        return $qb;
        
    }
    
    public function execute($query){
        
        $exec = $this->_conn->executeQuery($query);
        
        return $exec;
        
    }
    
    public function insertInit(){
        
        $qb = new InsertQueryRendering($this->_database);
        
        return $qb;
        
    }
    
    public function lastInsertId(){
        
        return $this->_conn->lastInsertId();
        
    }
    
}
