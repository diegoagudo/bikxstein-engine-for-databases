<?php

/**
 * Reponse monta e exibe a reponse da requisição
 *
 * @author Bruno Akio Kawakami
 */

namespace APIDB;

use APIDB\Helper\SecurityCrypt;

class Response {

    private $_app;
    private $_secretArray;
    private $_securityCrypt;
    public $responsePrependArray = array();
    public $encrypt = FALSE;
    public $statusNumber = 200;
    public $utf8_encode = FALSE;

    public function __construct(\Phlyty\App $app, $secretArray = array()) {
        $this->_app = $app;
        $this->_secretArray = $secretArray;
        $this->_securityCrypt = new SecurityCrypt;
    }

    private function cryptData($data) {

        if ($this->encrypt == TRUE) {

            $key = $this->_securityCrypt->pbkdf2($this->_secretArray['pass'], $this->_secretArray['salt'], 1000, 32);


            $returnData = $this->_securityCrypt->encrypt($data, $key);
        } else {
            $returnData = $data;
        }

        return $returnData;
    }

    public function set($prop, $value) {
        $this->$prop = $value;
        return $this;
    }

    public function utf8ArrayEncode($return){
        $contentReturn = null;
        
        if (is_array($return)) {
            if ($this->utf8_encode) {

                foreach ($return as $value) {

                    if (is_array($value)) {
                        $contentReturn[] = array_map('utf8_encode', $value);
                    } else {
                        $contentReturn[] = utf8_encode($value);
                    }
                }
            } else {
                foreach ($return as $value) {

                    $contentReturn[] = $value;
                }
            }
        } else {
            if ($this->utf8_encode) {
                $contentReturn = utf8_encode($return);
            } else {
                $contentReturn = $return;    
            }
        }
        
        return $contentReturn;
    }
    
    public function processDataReturn($return = null) {
        
        //$return = $this->utf8ArrayEncode($return);

        $status = $this->statusNumber;
        $this->response($this->prepareResponse($return), $status);
    }

    public function response($output, $status = 200, $content_type = 'application/json', $charset_type = 'UTF-8') {
        
        $this->_app->response()->setStatusCode($status);
        $this->_app->response()->getHeaders()->addHeaderLine('charset', $charset_type);
        $this->_app->response()->getHeaders()->addHeaderLine('Content-Type', $content_type);
        $this->_app->response()->setContent($this->cryptData(json_encode($output)));
        $this->_app->run();
        exit;
    }

    public function prepareResponse($dataReturn) {
        $messageReturns = array(
            100 => array(
                'status_information' => 'Continue',
                'status_description' => 'The requestor should continue with the request.'
            ),
            101 => array(
                'status_information' => 'Successful',
                'status_description' => 'The requester has asked the server to switch protocols and the server is acknowledging the information to then execute it.'
            ),
            200 => array(
                'status_information' => 'Successful',
                'status_description' => 'The server successfully processed the request.'
            ),
            201 => array(
                'status_information' => 'Created',
                'status_description' => 'The request was successful and the server created a new resource.'
            ),
            202 => array(
                'status_information' => 'Accepted',
                'status_description' => 'The server has accepted the request, but not yet processed.'
            ),
            203 => array(
                'status_information' => 'Information not authorize',
                'status_description' => 'The server successfully processed the request, but is returning information that may be from another source.'
            ),
            204 => array(
                'status_information' => 'Without content',
                'status_description' => 'O servidor processou a solicitação com sucesso, mas não está retornando nenhum conteúdo.'
            ),
            205 => array(
                'status_information' => 'Reconfigure content',
                'status_description' => 'The server successfully processed the request, but is not returning any content.'
            ),
            206 => array(
                'status_information' => 'Partial content',
                'status_description' => 'The server renders a partial GET request successfully.'
            ),
            400 => array(
                'status_information' => 'Invalid request',
                'status_description' => 'The server did not understand the syntax of the request.'
            ),
            401 => array(
                'status_information' => 'Unauthorized',
                'status_description' => 'The request requires authentication.'
            ),
            403 => array(
                'status_information' => 'Prohibited',
                'status_description' => 'The server refused the request.'
            ),
            404 => array(
                'status_information' => 'Not found',
                'status_description' => 'The server can not find the page you requested.'
            ),
            405 => array(
                'status_information' => 'Method not allowed',
                'status_description' => 'The method specified in the request is not allowed.'
            ),
        );

        if ($this->statusNumber) {
            
            $returnData = array(
                'status' => $this->statusNumber,
                'status_information' => $messageReturns[$this->statusNumber]['status_information'],
                'status_description' => $messageReturns[$this->statusNumber]['status_description']
            );

            if(is_array($this->responsePrependArray)){
                foreach ($this->responsePrependArray as $key => $value) {
                    $returnData[$key] = $value;
                }
            }
            
            if ($dataReturn) {
                $returnData['results'] = $dataReturn;
            }
            
            
        } else {
            
            $returnData = array(
                'status' => 200,
                'status_information' => $messageReturns[200]['status_information'],
                'status_description' => $messageReturns[200]['status_description'],
            );
            
            if(is_array($this->responsePrependArray)){
                foreach ($this->responsePrependArray as $key => $value) {
                    $returnData[$key] = $value;
                }
            }


            if ($dataReturn) {
                $returnData['results'] = $dataReturn;
            }
        }

        return $returnData;
    }

}
