<?php

return array(
    'security_keys' => array(
        'test' => array(
            'pass' => 'testpass',
            'salt' => 'testsalt',
            'token' => 'tokentest123'
        )
    ),
);

