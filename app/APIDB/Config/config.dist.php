<?php

return array(

    'environment' => 'test', // define type of environment for database connection. Points to the array named "db" in this file

    'encrypt'=>array(
        'response'=>false // true or false, default false
    ),

    'decrypt'=>array(
        'receive'=>false // true or false, default false
    ),


    'error_handling'=>array(
        'type'=>'dump', // dump or email
        'emailTo'=>'', // use only type defined to email, don't need smtp configuration
        'emailFrom'=>'' // use only type defined to email, don't need smtp configuration
    ),

    'db' => array(

        //environment type
        'test' => array(


            //this current names is used when you created some model
            'current_mysql' => array(
                'dbname' => 'test',
                'user' => 'root',
                'password' => '****',
                'host' => 'localhost',
                'driver' => 'pdo_mysql',
                'port' => 3306
            ),
            'current_postgres' => array(
                'dbname' => 'test',
                'user' => 'root',
                'password' => '****',
                'host' => 'localhost',
                'driver' => 'pdo_pgsql',
                'port' => 5432
            ),
            'current_oracle' => array(
                'dbname' => 'test',
                'user' => 'root',
                'password' => '****',
                'host' => 'localhost',
                'driver' => 'oci8', // need oracle instant client setup
                'port' => 1521
            ),
            'current_mssql' => array(
                'dbname' => 'test',
                'user' => 'root',
                'password' => '****',
                'host' => 'localhost',
                'driver' => 'pdo_sqlsrv',
                'port' => 1433
            ),

        ),

        //environment type
        'production' => array(


            //this current_* names is used when you created some model
            'current_mysql' => array(
                'dbname' => 'test',
                'user' => 'root',
                'password' => '****',
                'host' => 'localhost',
                'driver' => 'pdo_mysql',
                'port' => 3306
            ),
            'current_postgres' => array(
                'dbname' => 'test',
                'user' => 'root',
                'password' => '****',
                'host' => 'localhost',
                'driver' => 'pdo_pgsql',
                'port' => 5432
            ),
            'current_oracle' => array(
                'dbname' => 'test',
                'user' => 'root',
                'password' => '****',
                'host' => 'localhost',
                'driver' => 'oci8', // need oracle instant client setup
                'port' => 1521
            ),
            'current_mssql' => array(
                'dbname' => 'test',
                'user' => 'root',
                'password' => '****',
                'host' => 'localhost',
                'driver' => 'pdo_sqlsrv',
                'port' => 1433
            ),
        ),
    )
);
