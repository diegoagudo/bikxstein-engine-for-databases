<?php

namespace APIDB\Model;
/**
 *
 * @author Bruno Akio Kawakami
 */ 
class ContentTable extends AbstractModel {


    protected $_dbConn = "current_mysql"; // type of database connection, defined on /Config/config.php
    protected $_table = "current"; // table name
    protected $_key = "cod_current"; // primary key name
    protected $_debug = FALSE; // if this constant is TRUE the sql will be shown on screen, default false
    
    public function __construct($connArray) {
        parent::__construct($connArray[$this->_dbConn]);
    }
    
}

?>
