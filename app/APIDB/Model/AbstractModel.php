<?php

namespace APIDB\Model;

/**
 * Description of AbstractModel
 *
 * @author Bruno Akio Kawakami
 */
use APIDB\Helper\QueryRendering;

class AbstractModel {

    protected $_conn;
    protected $_columns = array('*');
    protected $_table;
    protected $_key;
    protected $_debug = FALSE;
    protected $_seq = null;
    protected $_database;

    public function __construct($connArray) {


        switch ($connArray['driver']) {
            case 'oci8':
            case 'pdo_oci':

                $this->_database = "oracle";

                break;

            case 'pdo_pgsql':

                $this->_database = "postgres";

                break;

            case 'pdo_mysql':

                $this->_database = "mysql";

                break;

            case 'pdo_sqlsrv':

                $this->_database = "mssql";

                break;

            default:
                break;
        }

        $conn = new QueryRendering($connArray, $this->_database);

        $this->_conn = $conn;
    }

    public function getByKey($keyValue = null, $order = 'DESC') {

        if ($keyValue) {

            $qb = $this->_conn->selectInit();

            $qb->select($this->_columns)
                ->from($this->_table, "AliasTable")
                ->orderBy($this->_key, $order)
                ->where($qb->expr()->orx(
                        $qb->expr()->eq('AliasTable.' . $this->_key, $keyValue)
                    )
                );

            $this->debug($qb);

            $stmt = $this->_conn->execute($qb->getSQL());

            return $stmt->fetchAll();
        } else {
            return FALSE;
        }
    }

    public function getAll($page = 1, $perPage = 15, $order = 'DESC') {

        $perPage = empty($perPage) ? 15 : $perPage;

        $page = empty($page) ? 1 : $page;

        switch ($this->_database) {
            case "oracle":

                $intersecValue = $perPage * $page;


                $offsetValue = ($intersecValue - $perPage);


                $limitValue = ($intersecValue - 1);

                if ($page <= $perPage) {
                    $limitValue = $intersecValue;
                }

                break;
            case "postgres":
            case "mysql":

                $offsetValue = $perPage * ($page - 1);


                $limitValue = $perPage;

                break;

            default:
                break;
        }


        $qb = $this->_conn->selectInit();
        $qbCount = $this->_conn->selectInit();

        $qb->select($this->_columns)
            ->from($this->_table, "AliasTable")
            ->setFirstResult($offsetValue)
            ->setMaxResults($limitValue)
            ->orderBy($this->_key, $order);

        $qbCount->select('COUNT(' . $this->_key . ') AS totalrow')
            ->from($this->_table, "AliasTable");

        $this->debug($qb);

        $stmt = $this->_conn->execute($qb->getSQL());

        $stmtCount = $this->_conn->execute($qbCount->getSQL());

        $dataRow = $stmtCount->fetchAll()[0];

        $dataRow['TOTALROW'] = empty($dataRow['TOTALROW']) ? null : $dataRow['TOTALROW'];

        $totalRow = $dataRow['TOTALROW'];

        $totalRow = empty($totalRow) ? null : $totalRow;

        $totalPage = ((real) $totalRow / $perPage);


        if (strpos($totalPage, ".")) {
            $totalPage = $totalPage + 1;
        }

        $returnArray['totalRow'] = (int) $totalRow;
        $returnArray['totalPages'] = (int) $totalPage;
        $returnArray['page'] = (int) $page;
        $returnArray['perPage'] = (int) $perPage;
        $returnArray['content'] = $stmt->fetchAll();

        if ($this->_database == "oracle") {
            if ($perPage == 1) {
                unset($returnArray['content'][1]);
            }
        }


        if (empty($returnArray['content'])) {
            $returnArray = null;
        }

        return $returnArray;
    }

    public function getByColumns($arrayColumns = array(), $type = 'AND', $latency = "EQUAL", $page = 1, $perPage = 15, $order = 'DESC') {



        $perPage = !is_numeric($perPage) ? 15 : $perPage;
        $page = !is_numeric($page) ? 1 : $page;

        switch ($this->_database) {
            case "oracle":

                $intersecValue = $perPage * $page;


                $offsetValue = ($intersecValue - $perPage);


                $limitValue = ($intersecValue - 1);

                if ($page <= $perPage) {
                    $limitValue = $intersecValue;
                }

                break;
            case "postgres":
            case "mysql":

                $offsetValue = $perPage * ($page - 1);


                $limitValue = $perPage;

                break;

            default:
                break;
        }

        if ($arrayColumns) {

            $qb = $this->_conn->selectInit();
            $qbCount = $this->_conn->selectInit();

            $qb->select($this->_columns)
                ->from($this->_table, "AliasTable")
                ->setFirstResult($offsetValue)
                ->setMaxResults($limitValue)
                ->orderBy($this->_key, $order);

            $qbCount->select('COUNT(' . $this->_key . ') AS totalrow')
                ->from($this->_table, "AliasTable");

            switch ($latency) {
                case 'EQUAL':

                    $queryLatency = "eq";

                    break;
                case 'LIKE':

                    $queryLatency = "like";

                    break;

                default:
                    break;
            }


            foreach ($arrayColumns as $columns => $value) {



                $value = $this->filterData($value, $queryLatency);


                if ($type == 'AND') {


                    $qb->andWhere($qb->expr()->orx(
                        $qb->expr()->$queryLatency('AliasTable.' . $columns, $value)
                    ));

                    $qbCount->andWhere($qbCount->expr()->orx(
                        $qbCount->expr()->$queryLatency('AliasTable.' . $columns, $value)
                    ));
                }
                if ($type == 'OR') {


                    $qb->orWhere($qb->expr()->orx(
                        $qb->expr()->$queryLatency('AliasTable.' . $columns, $value)
                    ));

                    $qbCount->orWhere($qbCount->expr()->orx(
                        $qbCount->expr()->$queryLatency('AliasTable.' . $columns, $value)
                    ));
                }
            }

            $this->debug($qb);

            $stmt = $this->_conn->execute($qb->getSQL());

            $stmtCount = $this->_conn->execute($qbCount->getSQL());

            $dataRow = $stmtCount->fetchAll()[0];

            $dataRow['TOTALROW'] = empty($dataRow['TOTALROW']) ? null : $dataRow['TOTALROW'];

            $totalRow = $dataRow['TOTALROW'];

            $totalRow = empty($totalRow) ? null : $totalRow;

            $totalPage = ((real) $totalRow / $perPage);

            if (strpos($totalPage, ".")) {
                $totalPage = $totalPage + 1;
            }

            $returnArray['totalRow'] = (int) $totalRow;
            $returnArray['totalPages'] = (int) $totalPage;
            $returnArray['page'] = (int) $page;
            $returnArray['perPage'] = (int) $perPage;
            $returnArray['content'] = $stmt->fetchAll();

            if ($this->_database == "oracle") {
                if ($perPage == 1) {
                    unset($returnArray['content'][1]);
                }
            }

            if (empty($returnArray['content'])) {
                $returnArray = null;
            }

            return $returnArray;
        } else {

            return FALSE;
        }
    }

    public function getDynamicRow($maxRow, $order = 'DESC') {
        $qb = $this->_conn->selectInit();

        $qb->select($maxRow)
            ->from($this->_table, "AliasTable");



        $this->debug($qb);

        $stmt = $this->_conn->execute($qb->getSQL());

        return $stmt->fetchAll();
    }

    public function save($arrayData) {



        switch ($this->_database) {
            case "oracle":
            case "mysql":

                $qb = $this->_conn->insertInit();
                $qb->insert($this->_table)->values($arrayData);


                $this->debug($qb);

                $this->_conn->execute($qb->getSQL());

                $lastId = $this->getDynamicRow('MAX(' . $this->_key . ')');

                $return = array_values($lastId[0]);

                break;

            case "postgres":

                $qb = $this->_conn->insertInit();
                $qb->insert($this->_table)->values($arrayData);


                $this->debug($qb);

                $stmt = $this->_conn->execute($qb->getSQL() . " RETURNING " . $this->_key);

                $return = $stmt->fetchAll()[0][$this->_key];

                break;


            default:
                $return = null;
                break;
        }

        return $return;
    }

    public function updateByKey($id, $dataArray = array()) {

        $qb = $this->_conn->updateInit();

        $qb->update($this->_table, "AliasTable");


        foreach ($dataArray as $key => $value) {


            $value = $this->filterData($value);

            $qb->set($key, $value);
        }


        $qb->andWhere($qb->expr()->orx(
            $qb->expr()->eq('AliasTable.' . $this->_key, $id)
        ));


        $this->debug($qb);

        $stmt = $this->_conn->execute($qb->getSQL());

        return $stmt;
    }

    public function updateByColumns($arrayColumns = array(), $dataArray = array()) {
        $qb = $this->_conn->updateInit();

        $qb->update($this->_table, "AliasTable");


        foreach ($dataArray as $key => $value) {



            $value = $this->filterData($value);

            $qb->set($key, $value);
        }


        foreach ($arrayColumns as $columns => $value) {




            $value = $this->filterData($value);

            $qb->andWhere($qb->expr()->orx(
                $qb->expr()->eq('AliasTable.' . $columns, $value)
            ));
        }


        $this->debug($qb);

        $stmt = $this->_conn->execute($qb->getSQL());

        return $stmt;
    }

    public function deleteByKey($keyValue) {

        if ($keyValue) {

            $qb = $this->_conn->selectInit();

            $qb->delete($this->_table, "AliasTable")
                ->where($qb->expr()->orx(
                        $qb->expr()->eq('AliasTable.' . $this->_key, $keyValue)
                    )
                );

            $this->debug($qb);

            $stmt = $this->_conn->execute($qb->getSQL());

            return $stmt;
        } else {
            return FALSE;
        }
    }

    public function deleteByColumns($arrayColumns) {
        if ($arrayColumns) {

            $qb = $this->_conn->selectInit();

            $qb->delete($this->_table, "AliasTable");

            foreach ($arrayColumns as $columns => $value) {

                if ($this->_database == 'oracle') {
                    if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/i", $value)) {

                        $value = "to_date('" . $value . "', 'yyyy-mm-dd hh24:mi:ss')";
                    }
                }

                if (is_string($value)) {
                    $value = "'" . $value . "'";
                }

                if (is_numeric($value)) {
                    $value = (real) $value;
                }

                $qb->andWhere($qb->expr()->orx(
                    $qb->expr()->eq('AliasTable.' . $columns, $value)
                ));
            }

            $this->debug($qb);

            $stmt = $this->_conn->execute($qb->getSQL());

            return $stmt;
        } else {

            return FALSE;
        }
    }

    public function debug($qb) {
        if ($this->_debug == TRUE) {
            echo "<pre>";
            var_dump($qb->getSQL());
            echo "</pre>";
            exit;
        }
    }

    protected function filterData($value, $queryLatency = null) {



        if ($this->_database == 'oracle') {
            if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/i", $value)) {

                $value = "to_date('" . $value . "', 'yyyy-mm-dd hh24:mi:ss')";

                return $value;
            }
        }

        if (is_string($value)) {

            if ($queryLatency == 'like') {
                $value = '%' . $value . '%';
            }

            $value = "'" . $value . "'";
        }

        if (is_numeric($value)) {

            if ($queryLatency == 'like') {
                $value = '%' . $value . '%';
            }

            $value = (real) $value;
        }

        return $value;
    }

}
