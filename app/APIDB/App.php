<?php

/**
 * App
 *
 * Classe para inicialização das rotas e execução delas.
 *
 * @author Bruno Akio Kawakami
 *
 */

namespace APIDB;

use APIDB\Routes,
    APIDB\Response,
    APIDB\Receive;

class App {

    private $_receive;
    private $_response;
    private $_routes;
    private $_phlyty;
    private $_environment;

    public function __construct() {

        $this->_receive = new Receive();
        $this->_routes = Routes::getRoutes();
        $this->_phlyty = new \Phlyty\App;

        $this->_response = new Response($this->_phlyty);

        
        $dataHeader = array(
            'client_security',
            'token'
        );

        $this->headerVerf($dataHeader);
    }

    public function run() {

        $config = $this->getConfig();

        if(empty($config)){
            $this->_response->set('statusNumber', 400)->processDataReturn('Invalid config settings');
        }

        if(!is_array($config)){
            $this->_response->set('statusNumber', 400)->processDataReturn('Invalid config settings');
        }
        
        $path = explode('/', $this->_receive->getURI());

        $route = '/' . $path[1];

        if (array_key_exists($route, $this->_routes)) {

            $securityKeys = $this->getSecurityKeys();
            
            $clientSecurity = $this->_receive->getHeader('client_security');

            $securityArray = $this->securityKeysVerf($securityKeys, $clientSecurity);

            $clientToken = $this->_receive->getHeader('token'); 
            
            if ($securityKeys['security_keys'][$clientSecurity]['token'] != $clientToken) {
                $arrayReturn = array('error' => 'Invalid security token');
                $this->_response->set('statusNumber', 400)->processDataReturn($arrayReturn);
            }


            $this->_environment = $connArray = $config['environment'];

            $connArray = $config['db'][$this->_environment];

            $class_name = 'APIDB\\Controller\\' . $this->_routes[$route];

            $load = new $class_name($this->_phlyty, $connArray, $securityArray, $this->_routes[$route]);

            $load->run();
        }else{
            $this->_response->set('statusNumber', 404)->processDataReturn(null);
        }
    }

    private function getConfig() {

        return include __DIR__ . '/Config/config.php';
    }

    private function getSecurityKeys() {

        return include __DIR__ . '/Security/security_keys.php';
    }

    private function headerVerf($data) {
        $returnArray = array();
        foreach ($data as $value) {
            if ($this->_receive->getHeader($value) === false) {
                $returnArray[] = $value;
            }
        }

        if (!empty($returnArray)) {

            $arrayReturn = array('error' => 'Invalid header senders', 'headers' => $returnArray);
            $this->_response->set('statusNumber', 401)->processDataReturn($arrayReturn);
            exit;
        }

        return TRUE;
    }

    private function securityKeysVerf($secKeys, $cliKeys) {
        
        if (!empty($secKeys['security_keys'][$cliKeys]) && !empty($secKeys['security_keys'][$cliKeys]['pass']) && !empty($secKeys['security_keys'][$cliKeys]['salt'])) {
            $securityArray = $secKeys['security_keys'][$cliKeys];
        } else {
            $arrayReturn = array('error' => 'Invalid security connection');
            $this->_response->set('statusNumber', 403)->processDataReturn($arrayReturn);
            exit;
        }

        return $securityArray;
    }

}
