<?php


namespace APIDB\Rules;

/**
 * Description of AbstractRules
 *
 * @author Bruno Akio Kawakami
 */
class AbstractRules
{


    public function __construct($dataContent)
    {
        $this->_dataContent = $dataContent;

        $keysValidation = array_keys($this->_dataValidation);

        $keysContent = array_keys($this->_dataContent);

        $resultKeyDiff = array_diff($keysContent, $keysValidation);

        foreach ($resultKeyDiff as $value) {
            unset($this->_dataContent[$value]);
        }
    }

    public function filterDataInsert($dataValidantion, $dataContent)
    {

        $requiredRows = array();
        $contentExceptions = array();
        $errorsReturn = array();

        foreach ($dataValidantion as $key => $value) {
            if ($value['null'] == false) {
                $requiredRows[] = $key;
            }
        }

        foreach ($dataContent as $keyContent => $valueContent) {


            if (!$dataValidantion[$keyContent]['type']($valueContent)) {

                if ($dataValidantion[$keyContent]['type'] == 'is_numeric') {
                    $appendError = "Este campo aceita apenas numérico";
                }

                if ($dataValidantion[$keyContent]['type'] == 'is_string') {
                    $appendError = "Este campo aceita apenas texto";
                }

                $contentExceptions[$keyContent]['type'] = "Tipo de campo inválido. " . $appendError;
            }

            if ($dataValidantion[$keyContent]['maxLength'] < strlen($valueContent)) {
                $contentExceptions[$keyContent]['maxLength'] = "Tamanho máximo de " . $dataValidantion[$keyContent]['maxLength'] . " caracteres";
            }

            $dataValidantion[$keyContent]['minLength'] = empty($dataValidantion[$keyContent]['minLength']) ? 0 : $dataValidantion[$keyContent]['minLength'];

            if ($dataValidantion[$keyContent]['minLength'] > strlen($valueContent)) {
                $contentExceptions[$keyContent]['minLength'] = "Tamanho minimo de " . $dataValidantion[$keyContent]['minLength'] . " caracteres";
            }

            if ($dataValidantion[$keyContent]['null'] == false && empty($valueContent)) {
                $contentExceptions[$keyContent]['null'] = "Valor não pode ser em branco";
            }
        }


        $keyDataContent = array_keys($dataContent);

        $diffArray = array_diff($requiredRows, $keyDataContent); //Conteúdo para erro caso não for empty

        if ($diffArray) {
            $errorsReturn['missing_data'] = array_values($diffArray);
        }
        if ($contentExceptions) {
            $errorsReturn['errors_data'] = $contentExceptions;
        }

        if (!$contentExceptions && !$diffArray) {
            $errorsReturn = FALSE;
        }

        return $errorsReturn;
    }

    public function filterDataUpdate($dataValidantion, $dataContent)
    {

        $contentExceptions = array();
        $errorsReturn = array();

        foreach ($dataContent as $keyContent => $valueContent) {


            if (!$dataValidantion[$keyContent]['type']($valueContent)) {
                $contentExceptions[$keyContent]['type'] = "Tipo de campo inválido";
            }

            if ($dataValidantion[$keyContent]['maxLength'] < strlen($valueContent)) {
                $contentExceptions[$keyContent]['maxLength'] = $dataValidantion[$keyContent]['maxLength'];
            }

            $dataValidantion[$keyContent]['minLength'] = empty($dataValidantion[$keyContent]['minLength']) ? 0 : $dataValidantion[$keyContent]['minLength'];

            if ($dataValidantion[$keyContent]['minLength'] > strlen($valueContent)) {
                $contentExceptions[$keyContent]['minLength'] = $dataValidantion[$keyContent]['minLength'];
            }

            if ($dataValidantion[$keyContent]['null'] == false && empty($valueContent)) {
                $contentExceptions[$keyContent]['null'] = "Valor não pode ser em branco";
            }
        }


        if ($contentExceptions) {
            $errorsReturn['errors_data'] = $contentExceptions;
        }

        if (!$contentExceptions) {
            $errorsReturn = FALSE;
        }

        return $errorsReturn;

    }

    public function insertRules() {

        $dataReturn = null;
        $errorsReturn = $this->filterDataInsert($this->_dataValidation, $this->_dataContent);

        if ($errorsReturn == false) {
            $dataReturn['dataReturn'] = $this->_dataContent;
            $dataReturn['error'] = FALSE;
        } else {
            $dataReturn['dataReturn'] = $errorsReturn;
            $dataReturn['error'] = TRUE;
        }

        return $dataReturn;
    }

    public function updateRules() {
        $dataReturn = null;
        $errorsReturn = $this->filterDataUpdate($this->_dataValidation, $this->_dataContent);

        if ($errorsReturn == false) {
            $dataReturn['dataReturn'] = $this->_dataContent;
            $dataReturn['error'] = FALSE;
        } else {
            $dataReturn['dataReturn'] = $errorsReturn;
            $dataReturn['error'] = TRUE;
        }

        return $dataReturn;
    }

}

?>
