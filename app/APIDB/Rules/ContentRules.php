<?php



namespace APIDB\Rules;

/**
 *
 * @author Bruno Akio Kawakami
 */
use APIDB\Rules\AbstractRules;

class ContentRules extends AbstractRules
{

    protected $_dataContent;
    protected $_dataValidation = array(
        'ROW1' => array( // name of table row
            'type' => 'is_string', // use only is_string or is_numeric
            'maxLength' => 16,
            'null' => false
        ),
        'ROW2' => array( // name of table row
            'type' => 'is_numeric', // use only is_string or is_numeric
            'maxLength' => 16,
            'null' => false
        ),
        'ROW3' => array( // name of table row
            'type' => 'is_numeric', // use only is_string or is_numeric
            'maxLength' => 16,
            'null' => false
        ),
    );

    public function __construct($dataContent)
    {
        parent::__construct($dataContent);
    }

}

?>
